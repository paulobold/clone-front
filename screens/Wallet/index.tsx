import * as React from 'react';
import { useState } from 'react';
import { Switch } from 'react-native';
import {
  Container,
  Title,
  Wallet,
  SubTitle,
  Bottons,
  Button,
  Description,
  Graph,
  Money,
  MoneyType,
  Value,
  ButtonText,
  BoxOption,
  OptionText,
  BoxCards,
  TitleSection,
  BoxItens,
  BoxItem,
  ItemDescriptionBox,
  CardTitle,
  CardDescr
} from './styles';
import { StatusBar } from 'expo-status-bar';
import EntIcon from 'react-native-vector-icons/Entypo';
import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';


export default function TabOneScreen() {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(!isEnabled);

  return (
    <Container>
      <StatusBar
        style="light"
        backgroundColor="#0bca69" />
      <Wallet>
        <Graph>
          <EntIcon
            name="bar-graph"
            color="white"
            size={15} />
        </Graph>
        <Title>Saldo PicPay</Title>
        <Money>
          <MoneyType>R$</MoneyType>
          <Value> 257.327,41</Value>
          <AntDesign
            name="eyeo"
            color="white"
            style={{ marginLeft: 10, marginTop: 6 }}
            size={22} />
        </Money>
        <Description>Seu saldo, de até R$ 250.000,00, está rendendo 210% do CDI</Description>

        <Bottons>
          <Button>
            <Fontisto
              name="dollar"
              color="white"
              size={15} />
            <ButtonText>Adicionar</ButtonText>
          </Button>
          <Button>
            <FontAwesome
              name="bank"
              color="white"
              size={15} />
            <ButtonText>Adicionar</ButtonText>
          </Button>
        </Bottons>
      </Wallet>
      <BoxOption>
        <OptionText>Usar saldo ao pagar</OptionText>
        <Switch
          trackColor={{ false: "#767577", true: "#4cd964" }}
          thumbColor={isEnabled ? "#fff" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
      </BoxOption>

      <BoxCards>
        <TitleSection>Formas de pagamento</TitleSection>

        <BoxItens>
          <BoxItem>
            <FontAwesome 
              name="cc-visa"
              color="#18146a"
              size={30}/>
              <ItemDescriptionBox>
                  <CardTitle>Visa</CardTitle>
                  <CardDescr>Cartão com o final 2453</CardDescr>
              </ItemDescriptionBox>
          </BoxItem>
          <BoxItem>
            <FontAwesome 
              name="cc-visa"
              color="#18146a"
              size={30}/>
              <ItemDescriptionBox>
                  <CardTitle>Visa</CardTitle>
                  <CardDescr>Cartão com o final 4782</CardDescr>
              </ItemDescriptionBox>
          </BoxItem>
        </BoxItens>
      </BoxCards>
    </Container>
  );
}

