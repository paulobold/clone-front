import { Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styled from 'styled-components';
import { Text, View } from '../../components/Themed'
import { LinearGradient } from 'expo-linear-gradient';

export const Container = styled(View)`
    flex: 1;
`;

export const Wallet = styled(LinearGradient).attrs({
    colors: ['#42db7c', '#0bb85c'],
    start:{ x: -1, y: 0 },
    end: { x: 1, y: 0 }
})`
    align-items: center;
    padding: 52px 13px 38px;
    position: relative;
`; 
export const Title = styled(Text)`
    color: white;
    font-family: Roboto_300Light;
    padding-bottom: 10px;
    font-size: 15px;
`; 
export const SubTitle = styled(Text)`
  
`;

export const Money = styled(View)`
    background-color: transparent;
    flex-direction: row;
    padding-bottom: 7px;
`;
export const MoneyType = styled(Text)`
    color: white;
    font-size: 35px;
    font-weight: 100;
    font-family: Roboto_300Light;
`;
export const Value = styled(Text)`
    color: white;
    font-size: 35px;
    font-family: Roboto_900Black;
`;

export const Description = styled(Text)`
    color: white;
    font-family: Roboto_300Light;
    padding: 0 10px 22px;
    text-align: center;
    font-size: 15px;
`;

export const Graph = styled(View)`
    border-radius: 100px;
    border-style: solid;
    border-color: #d7f3e3;
    border-width: 0.5px;
    padding: 8px;
    position: absolute;
    top: 38px;
    right: 15px;
    background-color: transparent;
`;


export const Bottons = styled(View)`
    flex-direction: row;
    background-color: transparent;
    justify-content: space-between;
    width: 100%;
`;

export const Button = styled(TouchableOpacity)`
    background-color: transparent;
    border-radius: 100px;
    border-style: solid;
    border-color: #d7f3e3;
    border-width: 2px;
    flex-direction: row;
    padding: 12px 15px;
    width: 160px;
    align-items: center;
    justify-content: center;
`;
export const ButtonText = styled(Text)`
    background-color: transparent;
    margin-left: 7px;
    font-size: 16px;
    color: white;
    font-family: Roboto_400Regular;
`;

export const BoxOption = styled(View)`
    elevation: 1;
    padding: 15px 15px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const OptionText = styled(Text)`
    font-size: 15px;
    font-family: Roboto_400Regular;
    color: #565655;
`;


export const BoxCards = styled(View)`
    padding: 15px;
`;

export const TitleSection = styled(Text)`
    font-size: 15px;
    font-family: Roboto_400Regular;
    color: #a3a4a4;
    padding-bottom: 14px;
`;

export const BoxItens = styled(View)``;
export const BoxItem = styled(View)`
    flex-direction: row;
    align-items: center;
    elevation: 1;
    border-radius: 10px;
    padding: 15px 15px;
    margin-bottom: 5px;
`;

export const ItemDescriptionBox = styled(View)`
    margin-left: 10px;

`;

export const CardTitle = styled(Text)`
    font-size: 14px;
    font-family: Roboto_400Regular;
    color: #565655;
    margin-bottom: 0;
    line-height: 15px;
`;
export const CardDescr = styled(Text)`
    font-size: 12px;
    font-family: Roboto_300Light;
    color: #a3a4a4;
    margin-top: 0;
`;