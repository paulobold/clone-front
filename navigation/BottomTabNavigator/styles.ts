import styled from 'styled-components';
import { Text, View } from '../../components/Themed';

export const PagarTitle = styled(Text)`
    font-size: 11px;
    color: white;
`;

export const BtnPagar = styled(View)`
    background-color: #1fb768;
    width: 58px;
    height: 58px;
    align-items: center;
    justify-content: center;
    border-radius: 100px;
    margin-bottom: 15px;
    elevation: 3;
`;

export const ItemMenu = styled(View)`
    align-items: center;
    justify-content: center;
    margin-top: 13px;
`;

export const ItemTitle = styled(Text)`
    font-family: Roboto_300Light;
    font-size: 11px;
    color: #34594f;
`;