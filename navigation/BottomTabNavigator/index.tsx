// import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { Text } from 'react-native';
import { View } from '../../components/Themed';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import Wallet from '../../screens/Wallet';
import TabTwoScreen from '../../screens/TabTwoScreen';
import { BottomTabParamList, TabOneParamList, TabTwoParamList } from '../../types';
import { BtnPagar, ItemMenu, ItemTitle, PagarTitle } from './styles';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AppLoading from 'expo-app-loading';
import {
  useFonts,
  Roboto_100Thin,
  Roboto_300Light,
  Roboto_400Regular,
  Roboto_900Black
} from '@expo-google-fonts/roboto';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  let [fontsLoaded] = useFonts({
    Roboto_100Thin,
    Roboto_300Light,
    Roboto_400Regular,
    Roboto_900Black
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <View style={{ flex: 1 }}>
      <BottomTab.Navigator
        initialRouteName="TabOne"
        tabBarOptions={{ activeTintColor: Colors[colorScheme].tint, style: { height: 55 } }}

      >
        <BottomTab.Screen
          name="TabOne"
          component={TabOneNavigator}
          options={{
            title: "",
            tabBarIcon: ({ color }) => (
              <ItemMenu>
                <Feather size={23} name="home" color={"#34594f"} />
                <ItemTitle>Início</ItemTitle>
              </ItemMenu>
            )
          }}
        />
        <BottomTab.Screen
          name="TabOn"
          component={TabOneNavigator}
          options={{
            tabBarBadgeStyle: { alignItems: 'center', justifyContent: 'center' },
            title: "",
            tabBarIcon: ({ color }) => (
              <ItemMenu>
                <FontAwesome5 size={23} name="wallet" color={"#34594f"} />
                <ItemTitle>Carteira</ItemTitle>
              </ItemMenu>
            )
          }}
        />

        <BottomTab.Screen
          name="Pagar"
          component={TabTwoNavigator}
          options={{
            title: "",
            tabBarLabel: "",
            tabBarIcon: ({ color }) => (
              <BtnPagar>
                <Feather size={19} name="dollar-sign" color={"white"} />
                <PagarTitle>Pagar</PagarTitle>
              </BtnPagar>),
          }}
        />
        <BottomTab.Screen
          name=" "
          component={TabTwoNavigator}
          options={{
            tabBarIcon: ({ color }) => (
              <ItemMenu>
              <Feather size={23} name="bell" color="#34594f" />
              <ItemTitle>Notificações</ItemTitle>
            </ItemMenu>
            ),
          }}
        />
        <BottomTab.Screen
          name="   "
          component={TabTwoNavigator}
          options={{
            tabBarIcon: ({ color }) => (
              <ItemMenu>
              <SimpleLineIcon size={22} name="bag" color="#34594f" />
              <ItemTitle>Store</ItemTitle>
            </ItemMenu>
            ),
          }}
        />


      </BottomTab.Navigator>
    </View>

    //   <BottomTab.Navigator
    //   initialRouteName="TabOne"
    //   tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
    //   <BottomTab.Screen
    //     name="TabOne"
    //     component={TabOneNavigator}
    //     options={{
    //       tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />,
    //     }}
    //   />

    // </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
// function TabBarIcon(props: { name: string; color: string }) {
//   return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
// }

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="Wallet"
        component={Wallet}
        options={{ headerTitle: 'Tab One Title', headerShown: false }}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTwoScreen"
        component={TabTwoScreen}
        options={{ headerTitle: 'Tab Two', headerShown: false }}
      />
    </TabTwoStack.Navigator>
  );
}
